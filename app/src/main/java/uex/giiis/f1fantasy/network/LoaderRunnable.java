package uex.giiis.f1fantasy.network;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.generatedPojos.ConstructorStandingsList;
import uex.giiis.f1fantasy.generatedPojos.ConstructorTable;
import uex.giiis.f1fantasy.generatedPojos.DriverStanding;
import uex.giiis.f1fantasy.generatedPojos.Formula1Calendar;
import uex.giiis.f1fantasy.generatedPojos.Formula1ConstructorStandings;
import uex.giiis.f1fantasy.generatedPojos.Formula1DriversConstructors;
import uex.giiis.f1fantasy.generatedPojos.Formula1Results;
import uex.giiis.f1fantasy.generatedPojos.Formula1Standings;
import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.generatedPojos.StandingsList;
import uex.giiis.f1fantasy.pojo.Information;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.ui.SplashActivity;

public class LoaderRunnable implements Runnable{

    private F1Database f1Database;
    private Context context;

    private final onStandingListListener mOnStandingListListener;

    private StandingsList standingsList;
    private ConstructorStandingsList constructorStandingsList;
    private List<ConstructorTable> listDriversConstructors = new ArrayList<ConstructorTable>();
    private List<ConstructorStandingsList> oldConstructorStandings = new ArrayList<ConstructorStandingsList>();
    private List<StandingsList> oldDriverStandings = new ArrayList<StandingsList>();
    private List<Race> results = new ArrayList<Race>();
    private List<Race> races = new ArrayList<Race>();

    public LoaderRunnable(F1Database f1Database, Context _context, onStandingListListener _mOnStandingListListener) {
        this.f1Database = f1Database;
        this.context = _context;
        this.mOnStandingListListener = _mOnStandingListListener;
    }

    @Override
    public void run() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://ergast.com/api/f1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        FormulaOneService service = retrofit.create(FormulaOneService.class);

        Information information = new Information();
        information.setSeason(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
        information.setRound(String.valueOf(-1));

        try {

            SharedPreferences sharedPreferences = context.getSharedPreferences("ultimaModificacion", context.MODE_PRIVATE);

            int ultimaDescargaSemana = sharedPreferences.getInt("ULTIMA_DESCARGA_SEMANA",-1);
            int ultimaDescargaAnio = sharedPreferences.getInt("ULTIMA_DESCARGA_ANIO",-1);

            int ultimaRondaActualizada = sharedPreferences.getInt("ULTIMA_RONDA_ACTUALIZADA",-1);

            int semana_actual = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
            int anio_actual = Calendar.getInstance().get(Calendar.YEAR);


            // formula1Standings
            Formula1Standings formula1Standings = service.getDriverStandings(information.getSeason()).execute().body();
            standingsList = formula1Standings.getMRData().getStandingsTable().getStandingsLists().get(0);


            //Actualizar la ronda actual del campeonato
            information.setRound(standingsList.getRound());

            if(anio_actual != ultimaDescargaAnio || ultimaDescargaAnio == -1 || ultimaDescargaSemana == -1) {

                if (ultimaRondaActualizada < Integer.parseInt(information.getRound())) {

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("ULTIMA_DESCARGA_SEMANA", semana_actual);
                    editor.putInt("ULTIMA_DESCARGA_ANIO", anio_actual);
                    editor.putInt("ULTIMA_RONDA_ACTUALIZADA", Integer.parseInt(information.getRound()));
                    editor.commit();

                    //F1 Calendar
                    Formula1Calendar formula1Calendar = service.getCalendar(information.getSeason()).execute().body();
                    races = formula1Calendar.getMRData().getRaceTable().getRaces();

                    //Formula1ConstructorStandings
                    Formula1ConstructorStandings formula1ConstructorStandings = service.getConstructorStandings(information.getSeason()).execute().body();
                    constructorStandingsList = formula1ConstructorStandings.getMRData().getStandingsTable().getStandingsLists().get(0);

                    //Formula1Results
                    Formula1Results formula1Results;
                    for (int i = 1; i <= Integer.parseInt(standingsList.getRound()); i++) {
                        formula1Results = service.getResult(information.getSeason(), i).execute().body();
                        results.add(formula1Results.getMRData().getRaceTable().getRaces().get(0));
                    }

                    //Formula1Standings
                    Formula1Standings oldFormula1Standings;
                    for (int i = 1; i < Integer.parseInt(standingsList.getRound()); i++) {
                        oldFormula1Standings = service.getPreviousDriverStandings(information.getSeason(), i).execute().body();
                        oldDriverStandings.add(oldFormula1Standings.getMRData().getStandingsTable().getStandingsLists().get(0));
                    }

                    //Formula1ConstructorStandings
                    Formula1ConstructorStandings oldFormula1ConstructorStandings;
                    for (int i = 1; i < Integer.parseInt(standingsList.getRound()); i++) {
                        oldFormula1ConstructorStandings = service.getPreviousConstructorStandings(information.getSeason(), i).execute().body();
                        oldConstructorStandings.add(oldFormula1ConstructorStandings.getMRData().getStandingsTable().getStandingsLists().get(0));
                    }

                    // Formula1DriversConstructors
                    Formula1DriversConstructors driversConstructors;
                    List<String> driversName = new ArrayList<String>();
                    List<DriverStanding> driverStandings = standingsList.getDriverStandings();
                    for (DriverStanding d : driverStandings) {
                        driversName.add(d.getDriver().getDriverId());
                    }
                    for (int i = 0; i < driversName.size(); i++) {
                        driversConstructors = service.getDriverConstructors(driversName.get(i)).execute().body();
                        listDriversConstructors.add(driversConstructors.getMRData().getConstructorTable());
                    }



                    AppExecutors.getInstance().mainThread().execute(new Runnable() {
                        @Override
                        public void run() {
                            mOnStandingListListener.onStandingListLoaded(standingsList);
                            mOnStandingListListener.onInformation(information);
                            mOnStandingListListener.onListDriversConstructors(listDriversConstructors);
                            mOnStandingListListener.onConstructorStandingsList(constructorStandingsList);
                            mOnStandingListListener.onListRaceResults(results);
                            mOnStandingListListener.onListRace(races);
                        }
                    });

                }
            }

            if ( context instanceof SplashActivity ) {
                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SplashActivity splashActivity = (SplashActivity) context;
                        splashActivity.finishSplash();
                    }
                });
            }


        } catch (IOException e) {
            Log.e("MYAPP", "exception: " + e.getMessage());
            Log.e("MYAPP", "exception: " + e.toString());
        }
    }

}
