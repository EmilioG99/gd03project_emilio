package uex.giiis.f1fantasy.repository;

import android.content.Context;

import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.pojo.DriverConstructors;
import uex.giiis.f1fantasy.roomdb.ConstructorDao;
import uex.giiis.f1fantasy.roomdb.ConstructorStandingDao;
import uex.giiis.f1fantasy.roomdb.DriverConstructorsDao;
import uex.giiis.f1fantasy.roomdb.DriverDao;
import uex.giiis.f1fantasy.roomdb.DriverStandingDao;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.roomdb.InformationDao;
import uex.giiis.f1fantasy.roomdb.LeagueDao;
import uex.giiis.f1fantasy.roomdb.RaceDao;
import uex.giiis.f1fantasy.roomdb.ResultDao;
import uex.giiis.f1fantasy.roomdb.UserDao;
import uex.giiis.f1fantasy.roomdb.UserLeaguesDao;

public class DaoCollection {

    public ConstructorDao mConstructorDao;
    public ConstructorStandingDao mConstructorStandingDao;
    public DriverConstructorsDao mDriverConstructorsDao;
    public DriverDao mDriverDao;
    public DriverStandingDao mDriverStandingDao;
    public InformationDao mInformationDao;
    public LeagueDao mLeagueDao;
    public RaceDao mRaceDao;
    public ResultDao mResultDao;
    public UserDao mUserDao;
    public UserLeaguesDao mUserLeaguesDao;
    public Context mContext;

    public DaoCollection (ConstructorDao constructorDao, ConstructorStandingDao constructorStandingDao,
                          DriverConstructorsDao driverConstructorsDao, DriverDao driverDao,
                          DriverStandingDao driverStandingDao, InformationDao informationDao,
                          LeagueDao leagueDao, RaceDao raceDao, ResultDao resultDao, UserDao userDao,
                          UserLeaguesDao userLeaguesDao, Context context) {

        mConstructorDao = constructorDao;
        mConstructorStandingDao = constructorStandingDao;
        mDriverConstructorsDao = driverConstructorsDao;
        mDriverDao = driverDao;
        mDriverStandingDao = driverStandingDao;
        mInformationDao = informationDao;
        mLeagueDao = leagueDao;
        mRaceDao = raceDao;
        mResultDao = resultDao;
        mUserDao = userDao;
        mUserLeaguesDao = userLeaguesDao;
        mContext = context;
    }

}
