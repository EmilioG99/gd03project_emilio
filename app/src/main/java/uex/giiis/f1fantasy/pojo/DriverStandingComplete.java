package uex.giiis.f1fantasy.pojo;

import androidx.room.Embedded;
import androidx.room.Relation;

import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.generatedPojos.DriverStanding;

public class DriverStandingComplete {

    @Embedded public DriverStanding driverStanding;
    @Relation(
            parentColumn = "idDriver",
            entityColumn = "driverId"
    )
    public Driver driver;
    @Relation(
            parentColumn = "idConstructor",
            entityColumn = "constructorId"
    )
    public Constructor constructor;

    public DriverStanding getDriverStanding() {
        return driverStanding;
    }

    public void setDriverStanding(DriverStanding driverStanding) {
        this.driverStanding = driverStanding;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Constructor getConstructor() {
        return constructor;
    }

    public void setConstructor(Constructor constructor) {
        this.constructor = constructor;
    }
}
