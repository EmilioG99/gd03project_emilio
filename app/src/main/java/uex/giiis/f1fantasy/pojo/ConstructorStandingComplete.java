package uex.giiis.f1fantasy.pojo;

import androidx.room.Embedded;
import androidx.room.Relation;

import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.ConstructorStanding;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.generatedPojos.DriverStanding;

public class ConstructorStandingComplete {

    @Embedded
    public ConstructorStanding constructorStanding;
    @Relation(
            parentColumn = "idConstructor",
            entityColumn = "constructorId"
    )
    public Constructor constructor;

    public ConstructorStanding getConstructorStanding() {
        return constructorStanding;
    }

    public void setConstructorStanding(ConstructorStanding constructorStanding) {
        this.constructorStanding = constructorStanding;
    }

    public Constructor getConstructor() {
        return constructor;
    }

    public void setConstructor(Constructor constructor) {
        this.constructor = constructor;
    }
}