package uex.giiis.f1fantasy.pojo;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import uex.giiis.f1fantasy.generatedPojos.Driver;

@Entity(indices = @Index(value = {"username"}, unique = true))
public class User {

    @PrimaryKey (autoGenerate = true)
    private int id;

    private String username;

    private String password;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
