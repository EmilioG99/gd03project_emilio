package uex.giiis.f1fantasy.pojo;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.ConstructorStanding;
import uex.giiis.f1fantasy.generatedPojos.Driver;

public class ConstructorComplete {

    @Embedded
    public Constructor constructor;
    @Relation(
            parentColumn = "constructorId",
            entityColumn = "idConstructor"
    )
    public List<Driver> driverList;

    public Constructor getConstructor() {
        return constructor;
    }

    public void setConstructor(Constructor constructor) {
        this.constructor = constructor;
    }

    public List<Driver> getDriverList() {
        return driverList;
    }

    public void setDriverList(List<Driver> driverList) {
        this.driverList = driverList;
    }
}