package uex.giiis.f1fantasy.pojo;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.Driver;

import static androidx.room.ForeignKey.CASCADE;

@Entity(primaryKeys = {"idDriver", "idConstructor"},
        foreignKeys = {@ForeignKey(entity = Driver.class,
            parentColumns = "driverId",
            childColumns = "idDriver",
            onDelete = CASCADE)})
public class DriverConstructors {

    @NonNull
    @SerializedName("@driverId")
    @Expose
    private String idDriver;

    @NonNull
    @SerializedName("@constructorId")
    @Expose
    private String idConstructor;

    @NonNull
    public String getIdDriver() {
        return idDriver;
    }

    public void setIdDriver(@NonNull String idDriver) {
        this.idDriver = idDriver;
    }

    @NonNull
    public String getIdConstructor() {
        return idConstructor;
    }

    public void setIdConstructor(@NonNull String idConstructor) {
        this.idConstructor = idConstructor;
    }
}
