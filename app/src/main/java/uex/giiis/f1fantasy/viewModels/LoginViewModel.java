package uex.giiis.f1fantasy.viewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.pojo.User;
import uex.giiis.f1fantasy.repository.F1Repository;
import uex.giiis.f1fantasy.ui.LoginActivity;
/**
 * {@link ViewModel} for {@link LoginActivity}
 */
public class LoginViewModel extends ViewModel {

    private final F1Repository mRepository;
    private LiveData<User> mUser;
    private String mUsername="";

    public LoginViewModel(F1Repository repository) {
        mRepository = repository;
        mUser = mRepository.getUser();
    }

    public void setUsername (String username){
        mUsername = username;
        mRepository.setUsername(username);
    }


    public LiveData<User> getUser() {
        return mUser;
    }

}