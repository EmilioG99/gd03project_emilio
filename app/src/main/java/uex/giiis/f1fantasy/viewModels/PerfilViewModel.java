package uex.giiis.f1fantasy.viewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.pojo.User;
import uex.giiis.f1fantasy.repository.F1Repository;
import uex.giiis.f1fantasy.ui.LoginActivity;
/**
 * {@link ViewModel} for {@link LoginActivity}
 */
public class PerfilViewModel extends ViewModel {

    private final F1Repository mRepository;
    private LiveData<User> mUser;
    private int mId = -1;

    public PerfilViewModel(F1Repository repository) {
        mRepository = repository;
        mUser = mRepository.getUserById();
    }

    public void setId (int id){
        mId = id;
        mRepository.setId(id);
    }

    public LiveData<User> getUser() {
        return mUser;
    }

    public int updateUser(int id, String username, String password) {
        User u = new User();
        u.setId(id);
        u.setUsername(username);
        u.setPassword(password);
        return mRepository.updateUser(u);
    }

    public int deleteUser(int id) {
        int r = mRepository.deleteUser(id);
        mRepository.deleteUserLeagues(id);
        return r;
    }

}