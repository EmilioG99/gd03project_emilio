package uex.giiis.f1fantasy.viewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import uex.giiis.f1fantasy.pojo.ConstructorStandingComplete;
import uex.giiis.f1fantasy.pojo.UserLeaguesComplete;
import uex.giiis.f1fantasy.repository.F1Repository;
import uex.giiis.f1fantasy.ui.VerPilotosElegidosActivity;

/**
 * {@link ViewModel} for {@link VerPilotosElegidosActivity}
 */
public class VerPilotosElegidosViewModel extends ViewModel {

    private final F1Repository mRepository;
    private LiveData<List<UserLeaguesComplete>> mUserLeaguesComplete;
    private int mIdLiga;

    public VerPilotosElegidosViewModel(F1Repository repository) {
        mRepository = repository;
        mUserLeaguesComplete = mRepository.getFromLeague();
    }

    public LiveData<List<UserLeaguesComplete>> getFromLeague() {
        return mUserLeaguesComplete;
    }

    public void setIdLiga(int idLiga){
        mIdLiga = idLiga;
        mRepository.setIdLiga(idLiga);
    }

}