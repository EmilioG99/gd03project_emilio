package uex.giiis.f1fantasy.ui.clasificaciones;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.generatedPojos.DriverStanding;
import uex.giiis.f1fantasy.network.F1NetworkDataSource;
import uex.giiis.f1fantasy.pojo.ConstructorComplete;
import uex.giiis.f1fantasy.pojo.ConstructorStandingComplete;
import uex.giiis.f1fantasy.pojo.DriverStandingComplete;
import uex.giiis.f1fantasy.recyclerViews.ClasConstructoresRecyclerViewAdapter;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.ConstructorStanding;
import uex.giiis.f1fantasy.repository.F1Repository;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.viewModels.ClasificacionesConstructoresViewModel;
import uex.giiis.f1fantasy.viewModels.EscuderiasViewModel;

;

public class ClasificacionesConstructores extends Fragment {

    private ClasConstructoresRecyclerViewAdapter adapter;
    private List<ConstructorStanding> constructorStandings;
    private List<Constructor> constructors;
    private List<ConstructorStandingComplete> constructorStandingCompletes;
    private F1Repository mRepository;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        constructorStandings = new ArrayList<>();
        constructorStandingCompletes = new ArrayList<>();
        adapter = new ClasConstructoresRecyclerViewAdapter(constructorStandings, getContext());
        constructorStandingsDatabase();

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.clasificacion_constructores, container, false);

        RecyclerView recyclerView;
        RecyclerView.LayoutManager layoutManager;

        recyclerView = (RecyclerView) view.findViewById(R.id.list_constructores_clasificacion);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    private void actualizarRecyclerView(List<ConstructorStanding> updateConstructorStandings) {
        adapter.swap(updateConstructorStandings);

    }

    public void constructorStandingsDatabase () {

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        ClasificacionesConstructoresViewModel mViewModel = new ViewModelProvider(this, appContainer.clasificacionesConstructoresFactory)
                .get(ClasificacionesConstructoresViewModel.class);
        mViewModel.getConstructorStandingsCompleteList().observe(this, new Observer<List<ConstructorStandingComplete>>() {

            @Override
            public void onChanged(List<ConstructorStandingComplete> constructorStandingList) {
                constructorStandings.clear();
                constructorStandingCompletes = constructorStandingList;

                for (ConstructorStandingComplete cs : constructorStandingCompletes) {
                    cs.getConstructorStanding().setConstructor(cs.getConstructor());
                    constructorStandings.add(cs.getConstructorStanding());
                }

                Collections.sort(constructorStandings,new Comparator<ConstructorStanding>(){
                    @Override
                    public int compare(final ConstructorStanding lhs,ConstructorStanding rhs) {
                        if(Integer.parseInt(lhs.getPosition()) < Integer.parseInt(rhs.getPosition())) {
                            return -1;
                        } else
                            return 1;
                    }
                });

                actualizarRecyclerView(constructorStandings);
            }
        });

    }

}