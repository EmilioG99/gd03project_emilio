package uex.giiis.f1fantasy.ui.clasificaciones;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.MainActivity;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.network.F1NetworkDataSource;
import uex.giiis.f1fantasy.pojo.DriverStandingComplete;
import uex.giiis.f1fantasy.pojo.Information;
import uex.giiis.f1fantasy.recyclerViews.ClasPilotosRecyclerViewAdapter;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.generatedPojos.DriverStanding;
import uex.giiis.f1fantasy.repository.F1Repository;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.viewModels.ClasificacionesPilotosViewModel;;

public class ClasificacionesPilotos extends Fragment {

    private ClasPilotosRecyclerViewAdapter adapter;
    private List<DriverStanding> driverStandings;
    private List<DriverStandingComplete> driverStandingCompletes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        driverStandings = new ArrayList<>();
        driverStandingCompletes = new ArrayList<>();
        adapter = new ClasPilotosRecyclerViewAdapter(driverStandings, getContext());
        driverStandingsDatabase();

    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.clasificacion_pilotos, container, false);

        RecyclerView recyclerView;
        RecyclerView.LayoutManager layoutManager;

        recyclerView = (RecyclerView) view.findViewById(R.id.list_pilotos_clasificacion);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    private void actualizarRecyclerView(List<DriverStanding> updateDriversStanding) {
        adapter.swap(updateDriversStanding);

    }

    public void driverStandingsDatabase () {

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        ClasificacionesPilotosViewModel mViewModel = new ViewModelProvider(this, appContainer.clasificacionesPilotosFactory)
                .get(ClasificacionesPilotosViewModel.class);
        mViewModel.getDriverStandingComplete().observe(this, new Observer<List<DriverStandingComplete>>() {
            @Override
            public void onChanged(List<DriverStandingComplete> driverStandingList) {
                driverStandings.clear();
                driverStandingCompletes = driverStandingList;
                for (DriverStandingComplete ds : driverStandingCompletes) {
                    (ds.getDriverStanding()).setDriver(ds.getDriver());
                    List<Constructor> listCons = new ArrayList<>();
                    listCons.add(ds.getConstructor());
                    (ds.getDriverStanding()).setConstructors(listCons);

                    driverStandings.add(ds.getDriverStanding());

                }

                Collections.sort(driverStandings,new Comparator<DriverStanding>(){
                    @Override
                    public int compare(final DriverStanding lhs,DriverStanding rhs) {
                        if(Integer.parseInt(lhs.getPosition()) < Integer.parseInt(rhs.getPosition())) {
                            return -1;
                        } else
                            return 1;
                    }
                });

                actualizarRecyclerView(driverStandings);

            }
        });

    }

}