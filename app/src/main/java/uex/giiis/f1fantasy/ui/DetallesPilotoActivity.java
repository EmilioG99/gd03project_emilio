package uex.giiis.f1fantasy.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.Helper;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.pojo.DriverComplete;
import uex.giiis.f1fantasy.pojo.DriverConstructors;
import uex.giiis.f1fantasy.pojo.User;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.viewModels.DetallesPilotoViewModel;
import uex.giiis.f1fantasy.viewModels.RegisterViewModel;

import static uex.giiis.f1fantasy.ColeccionFotos.obtenerFondoPiloto;
import static uex.giiis.f1fantasy.ColeccionFotos.obtenerImagenPiloto;
import static uex.giiis.f1fantasy.ColeccionFotos.obtenerNacionalidad;

public class DetallesPilotoActivity extends AppCompatActivity {

    List<DriverConstructors> driverConstructors;
    Driver d;
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalle_piloto);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        Bundle bundle = getIntent().getExtras();
        String id_piloto = bundle.getString("ID_PILOTO");

        TextView nombre = (TextView) findViewById(R.id.nombre_piloto);
        TextView fecha_nacimiento = (TextView) findViewById(R.id.texto_fecha_nacimiento);
        TextView nacionalidad = (TextView) findViewById(R.id.texto_nacionalidad);
        TextView constructor_actual = (TextView) findViewById(R.id.texto_constructor_actual);
        ImageView imagen_fondo = (ImageView) findViewById(R.id.imagen_fondo);
        ImageView bandera_nacionalidad = (ImageView) findViewById(R.id.bandera_nacionalidad);
        ImageView imagen_piloto = (ImageView) findViewById(R.id.imagen_piloto);
        ListView lista_constructores = (ListView) findViewById(R.id.list_constructores_piloto);
        TextView titulo_escuderias_anteriores = (TextView) findViewById(R.id.titulo_escuderias_anteriores);

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        DetallesPilotoViewModel mViewModel = new ViewModelProvider(this, appContainer.detallesPilotoFactory)
                .get(DetallesPilotoViewModel.class);
        mViewModel.setDriverId(id_piloto);
        mViewModel.getDriverComplete().observe(this, new Observer<DriverComplete>() {

            @Override
            public void onChanged(DriverComplete driverComplete) {
                String[] values = new String[driverComplete.getDriverConstructors().size()-1];

                int j = 0;
                for (int i = 0; i < driverComplete.getDriverConstructors().size(); i++) {
                    if(!driverComplete.getDriverConstructors().get(i).getIdConstructor().equals(driverComplete.getDriver().getIdConstructor())){
                        values[j] = driverComplete.getDriverConstructors().get(i).getIdConstructor().replace("_", " ").toUpperCase();
                        j++;
                    }
                }

                d = driverComplete.getDriver();

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(DetallesPilotoActivity.this, R.layout.constructores_piloto_item, R.id.construcores_piloto_text, values);
                nombre.setText(d.getGivenName()+" "+d.getFamilyName());
                fecha_nacimiento.setText(d.getDateOfBirth());
                nacionalidad.setText(d.getNationality());
                bandera_nacionalidad.setImageResource(obtenerNacionalidad(d.getNationality()));
                imagen_piloto.setImageResource(obtenerImagenPiloto(id_piloto));
                imagen_fondo.setImageResource(obtenerFondoPiloto(id_piloto));
                constructor_actual.setText(driverComplete.getConstructor().getName());
                lista_constructores.setAdapter(adapter);

                if(values.length==0){
                    titulo_escuderias_anteriores.setText("");
                }

                Helper.getListViewSize(lista_constructores);
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_app_bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    public void visitarEnlace(View view){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(d.getUrl()));
        startActivity(browserIntent);
    }

    public void cerrarSesion (MenuItem item) {
        sharedPref = DetallesPilotoActivity.this.getSharedPreferences("usuario", DetallesPilotoActivity.this.MODE_PRIVATE);

        int id_usuario = sharedPref.getInt("id_usuario", -1);

        if (id_usuario != -1) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt("id_usuario", -1);
            editor.commit();

            Intent intent = new Intent (this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    public void editarPerfil (MenuItem item) {
        Intent intent = new Intent(this, PerfilActivity.class);
        startActivity(intent);
    }

}