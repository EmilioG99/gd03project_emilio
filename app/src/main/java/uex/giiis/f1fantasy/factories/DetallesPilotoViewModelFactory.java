package uex.giiis.f1fantasy.factories;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import uex.giiis.f1fantasy.repository.F1Repository;
import uex.giiis.f1fantasy.viewModels.DetallesPilotoViewModel;

/**
 * Factory method that allows us to create a ViewModel with a constructor that takes a
 * {@link F1Repository}
 */
public class DetallesPilotoViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final F1Repository mRepository;

    public DetallesPilotoViewModelFactory(F1Repository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new DetallesPilotoViewModel(mRepository);
    }
}