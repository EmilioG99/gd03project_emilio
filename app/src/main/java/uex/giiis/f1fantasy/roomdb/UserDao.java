package uex.giiis.f1fantasy.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import uex.giiis.f1fantasy.pojo.User;

@Dao
public interface UserDao {

    @Query("SELECT * FROM user")
    public List<User> getAll();

    @Query("SELECT * FROM user WHERE id = :userId")
    public User get(int userId);

    @Query("SELECT * FROM user WHERE username = :name")
    public LiveData<User> get(String name);

    @Query("SELECT * FROM user WHERE id = :idUser")
    public LiveData<User> getById(int idUser);

    @Query("SELECT * FROM user WHERE id = :idUser")
    public User getUserId(int idUser);

    @Query("SELECT * FROM user WHERE username = :name and password = :password")
    public LiveData<User> getUserPassword(String name, String password);

    @Insert(onConflict = OnConflictStrategy.ABORT)
    public long insert (User user);

    @Update
    public int update (User user);

    @Query("UPDATE user SET username = :user, password = :pass WHERE id = :idUser")
    public int update (int idUser, String user, String pass);

    @Delete
    public int delete (User user);

    @Query("DELETE FROM user WHERE id = :idUser")
    public int delete (int idUser);
}