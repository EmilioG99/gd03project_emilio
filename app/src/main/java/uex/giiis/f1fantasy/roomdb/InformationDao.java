package uex.giiis.f1fantasy.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import uex.giiis.f1fantasy.pojo.Information;

@Dao
public interface InformationDao {

    @Query("SELECT * FROM information")
    public Information get();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert (Information information);

    @Update
    public int update (Information information);

    @Query("UPDATE INFORMATION SET round = :round")
    public int update (String round);

    @Query("DELETE FROM information")
    public void delete ();
}
