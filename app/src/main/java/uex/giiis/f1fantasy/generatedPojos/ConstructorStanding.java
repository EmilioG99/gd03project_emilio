
package uex.giiis.f1fantasy.generatedPojos;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;

@Entity(primaryKeys = {"round", "idConstructor"},
        foreignKeys = {@ForeignKey(entity = Constructor.class,
                parentColumns = "constructorId",
                childColumns = "idConstructor",
                onDelete = CASCADE)})
public class ConstructorStanding {

    @SerializedName("position")
    @Expose
    private String position;
    @Ignore
    @SerializedName("positionText")
    @Expose
    private String positionText;
    @SerializedName("points")
    @Expose
    private String points;
    @SerializedName("wins")
    @Expose
    private String wins;
    @Ignore
    @SerializedName("Constructor")
    @Expose
    private Constructor constructor;

    @NonNull
    private String idConstructor;

    @NonNull
    private String round;

    public String getIdConstructor() {
        return idConstructor;
    }

    public void setIdConstructor(String idConstructor) {
        this.idConstructor = idConstructor;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    @JsonProperty("position")
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @JsonProperty("positionText")
    public String getPositionText() {
        return positionText;
    }

    public void setPositionText(String positionText) {
        this.positionText = positionText;
    }

    @JsonProperty("points")
    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    @JsonProperty("wins")
    public String getWins() {
        return wins;
    }

    public void setWins(String wins) {
        this.wins = wins;
    }

    @JsonProperty("Constructor")
    public Constructor getConstructor() {
        return constructor;
    }

    public void setConstructor(Constructor constructor) {
        this.constructor = constructor;
    }

}
