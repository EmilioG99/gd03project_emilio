
package uex.giiis.f1fantasy.generatedPojos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConstructorStandingsTable {

    @SerializedName("season")
    @Expose
    private String season;
    @SerializedName("StandingsLists")
    @Expose
    private List<ConstructorStandingsList> constructorStandingsLists = null;

    @JsonProperty("season")
    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    @JsonProperty("StandingsLists")
    public List<ConstructorStandingsList> getStandingsLists() {
        return constructorStandingsLists;
    }

    public void setStandingsLists(List<ConstructorStandingsList> standingsLists) {
        this.constructorStandingsLists = standingsLists;
    }

}
