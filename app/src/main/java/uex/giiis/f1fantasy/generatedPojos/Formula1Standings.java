
package uex.giiis.f1fantasy.generatedPojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Formula1Standings {

    @SerializedName("MRData")
    @Expose
    private MRDataStandings mRDataStandings;

    @JsonProperty("MRData")
    public MRDataStandings getMRData() {
        return mRDataStandings;
    }

    public void setMRData(MRDataStandings mRDataStandings) {
        this.mRDataStandings = mRDataStandings;
    }

}
