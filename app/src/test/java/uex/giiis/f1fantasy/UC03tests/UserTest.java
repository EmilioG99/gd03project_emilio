package uex.giiis.f1fantasy.UC03tests;

import org.junit.Test;

import uex.giiis.f1fantasy.pojo.User;

import static org.junit.Assert.assertEquals;


public class UserTest {

    private User user;

    @Test
    public void userShouldBeCreated() {

        user = new User();
        user.setId(1);
        user.setUsername("Arturo");
        user.setPassword("123321");

        assertEquals(1, user.getId());
        assertEquals("Arturo", user.getUsername());
        assertEquals("123321", user.getPassword());
    }
}
