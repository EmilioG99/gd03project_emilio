package uex.giiis.f1fantasy.UC13tests;

import uex.giiis.f1fantasy.generatedPojos.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @see <a href="http://d.android.com/tools/testing%22%3ETesting documentation</a>
 */
public class ResultTest {

    private Result result;

    @Test
    public void resultShouldBeCreated() {
        result = new Result();

        result.setPosition("18");
        result.setPoints("0");
        result.setStatus("DNF");
        result.setRound("16");
        result.setIdDriver("latifi");
        result.setIdConstructor("williams");

        assertEquals("18", result.getPosition());
        assertEquals("0",result.getPoints());
        assertEquals("DNF", result.getStatus());
        assertEquals("16", result.getRound());
        assertEquals("latifi", result.getIdDriver());
        assertEquals("williams", result.getIdConstructor());

    }
}