package uex.giiis.f1fantasy.UC08tests;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Circuit;
import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.ConstructorStanding;
import uex.giiis.f1fantasy.generatedPojos.ConstructorStandingsList;
import uex.giiis.f1fantasy.generatedPojos.ConstructorStandingsTable;
import uex.giiis.f1fantasy.generatedPojos.ConstructorTable;
import uex.giiis.f1fantasy.generatedPojos.DriverStanding;
import uex.giiis.f1fantasy.generatedPojos.Formula1Calendar;
import uex.giiis.f1fantasy.generatedPojos.Formula1ConstructorStandings;
import uex.giiis.f1fantasy.generatedPojos.Formula1DriversConstructors;
import uex.giiis.f1fantasy.generatedPojos.Formula1Results;
import uex.giiis.f1fantasy.generatedPojos.Formula1Standings;
import uex.giiis.f1fantasy.generatedPojos.Location;
import uex.giiis.f1fantasy.generatedPojos.MRDataCalendar;
import uex.giiis.f1fantasy.generatedPojos.MRDataConstructorStandings;
import uex.giiis.f1fantasy.generatedPojos.MRDataDriversConstructors;
import uex.giiis.f1fantasy.generatedPojos.MRDataResults;
import uex.giiis.f1fantasy.generatedPojos.MRDataStandings;
import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.generatedPojos.RaceTable;
import uex.giiis.f1fantasy.generatedPojos.Result;
import uex.giiis.f1fantasy.generatedPojos.StandingsList;
import uex.giiis.f1fantasy.generatedPojos.StandingsTable;
import uex.giiis.f1fantasy.network.FormulaOneService;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class ApiServiceUnitTest {

    @Test
    public void getDriversStandingTest() throws IOException {

        //Step1: we create Driver Standings
        List<DriverStanding> driverStandingList = new ArrayList<>();
        DriverStanding driverStanding1 = new DriverStanding();
        DriverStanding driverStanding2 = new DriverStanding();
        DriverStanding driverStanding3 = new DriverStanding();

        driverStanding1.setPosition("1");
        driverStanding1.setWins("1");
        driverStanding1.setPoints("25");
        driverStanding1.setIdDriver("1");
        driverStanding1.setRound("1");
        driverStandingList.add(driverStanding1);

        driverStanding2.setPosition("2");
        driverStanding2.setWins("0");
        driverStanding2.setPoints("18");
        driverStanding2.setIdDriver("2");
        driverStanding2.setRound("1");
        driverStandingList.add(driverStanding2);

        driverStanding3.setPosition("3");
        driverStanding3.setWins("0");
        driverStanding3.setPoints("15");
        driverStanding3.setIdDriver("3");
        driverStanding3.setRound("1");
        driverStandingList.add(driverStanding3);

        List<StandingsList> standingsListList = new ArrayList<>();
        StandingsList standingsList1 = new StandingsList();
        standingsList1.setRound("1");
        standingsList1.setSeason("2020");
        standingsList1.setDriverStandings(driverStandingList);
        standingsListList.add(standingsList1);

        StandingsTable standingsTable = new StandingsTable();
        standingsTable.setSeason("2020");
        standingsTable.setStandingsLists(standingsListList);

        MRDataStandings mRDataStandings = new MRDataStandings();
        mRDataStandings.setStandingsTable(standingsTable);
        mRDataStandings.setSeries("f1");

        Formula1Standings formula1Standings = new Formula1Standings();
        formula1Standings.setMRData(mRDataStandings);


        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        MockWebServer mockWebServer = new MockWebServer();
        mockWebServer.start();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only getCenters request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(formula1Standings));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        FormulaOneService service = retrofit.create(FormulaOneService.class);


        //Step6: we create the call to get driver standings
        Call<Formula1Standings> call = service.getDriverStandings("2020");
        // and we execute the call
        Response<Formula1Standings> response = call.execute();

        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        Formula1Standings standingsResponse = response.body();
        assertEquals(standingsResponse.getMRData().getStandingsTable().getSeason(), "2020");
        assertEquals(standingsResponse.getMRData().getStandingsTable().getStandingsLists().size(), 1);
        assertEquals(standingsResponse.getMRData().getStandingsTable().getStandingsLists().get(0).getDriverStandings().size(), 3);
        assertEquals(standingsResponse.getMRData().getStandingsTable().getStandingsLists().get(0).getDriverStandings().
                get(1).getPoints(), driverStanding2.getPoints());

        //Step9: Finish web server
        mockWebServer.shutdown();
    }

    @Test
    public void getConstructorStandingTest() throws IOException {

        //Step1: Create instances
        List<ConstructorStanding> constructorStandingList = new ArrayList<>();
        ConstructorStanding constructorStanding1 = new ConstructorStanding();
        ConstructorStanding constructorStanding2 = new ConstructorStanding();
        ConstructorStanding constructorStanding3 = new ConstructorStanding();

        constructorStanding1.setRound("1");
        constructorStanding1.setPoints("50");
        constructorStanding1.setWins("3");
        constructorStanding1.setIdConstructor("1");
        constructorStanding1.setPosition("4");
        constructorStandingList.add(constructorStanding1);

        constructorStanding2.setRound("1");
        constructorStanding2.setPoints("30");
        constructorStanding2.setWins("2");
        constructorStanding2.setIdConstructor("4");
        constructorStanding2.setPosition("7");
        constructorStandingList.add(constructorStanding2);

        constructorStanding3.setRound("1");
        constructorStanding3.setPoints("25");
        constructorStanding3.setWins("2");
        constructorStanding3.setIdConstructor("6");
        constructorStanding3.setPosition("11");
        constructorStandingList.add(constructorStanding3);

        List<ConstructorStandingsList> constructorStandingsListList = new ArrayList<>();
        ConstructorStandingsList constructorStandingsList1 = new ConstructorStandingsList();
        constructorStandingsList1.setRound("1");
        constructorStandingsList1.setSeason("2020");
        constructorStandingsList1.setConstructorStandings(constructorStandingList);
        constructorStandingsListList.add(constructorStandingsList1);

        ConstructorStandingsTable constructorStandingsTable = new ConstructorStandingsTable();
        constructorStandingsTable.setSeason("2020");
        constructorStandingsTable.setStandingsLists(constructorStandingsListList);

        MRDataConstructorStandings mrDataConstructorStandings = new MRDataConstructorStandings();
        mrDataConstructorStandings.setStandingsTable(constructorStandingsTable);
        mrDataConstructorStandings.setSeries("f1");

        Formula1ConstructorStandings formula1ConstructorStandings = new Formula1ConstructorStandings();
        formula1ConstructorStandings.setMRData(mrDataConstructorStandings);


        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        MockWebServer mockWebServer = new MockWebServer();
        mockWebServer.start();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only getCenters request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(formula1ConstructorStandings));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        FormulaOneService service = retrofit.create(FormulaOneService.class);


        //Step6: we create the call to get constructor standings
        Call<Formula1ConstructorStandings> call = service.getConstructorStandings("2020");
        // and we execute the call
        Response<Formula1ConstructorStandings> response = call.execute();

        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        Formula1ConstructorStandings standingsResponse = response.body();
        assertEquals(standingsResponse.getMRData().getStandingsTable().getSeason(), "2020");
        assertEquals(standingsResponse.getMRData().getStandingsTable().getStandingsLists().size(), 1);
        assertEquals(standingsResponse.getMRData().getStandingsTable().getStandingsLists().get(0).getConstructorStandings().size(), 3);
        assertEquals(standingsResponse.getMRData().getStandingsTable().getStandingsLists().get(0).getConstructorStandings().
                get(1).getPoints(), constructorStanding2.getPoints());


        //Step9: Finish web server
        mockWebServer.shutdown();

    }


    @Test
    public void getResult() throws IOException {

        //Step1: Create instances
        List<Result> resultList = new ArrayList<>();
        Result result1 = new Result();
        Result result2 = new Result();
        Result result3 = new Result();
        Result result4 = new Result();

        result1.setPosition("1");
        result1.setPoints("40");
        result1.setIdDriver("hamilton");
        resultList.add(result1);

        result2.setPosition("2");
        result2.setPoints("30");
        result2.setIdDriver("verstappen");
        resultList.add(result2);

        result3.setPosition("3");
        result3.setPoints("20");
        result3.setIdDriver("sainz");
        resultList.add(result3);

        result4.setPosition("4");
        result4.setPoints("10");
        result4.setIdDriver("grosjean");
        resultList.add(result4);

        Location location = new Location();
        location.setCountry("Austria");
        location.setLocality("Spielburg");

        Circuit circuit = new Circuit();
        circuit.setCircuitName("Red Bull Ring");
        circuit.setCircuitId("red_bull_ring");
        circuit.setLocation(location);

        List<Race> raceList = new ArrayList<>();
        Race race = new Race();
        race.setRound("2");
        race.setCircuit(circuit);
        race.setResults(resultList);
        raceList.add(race);

        RaceTable raceTable = new RaceTable();
        raceTable.setSeason("2020");
        raceTable.setRound("2");
        raceTable.setRaces(raceList);

        MRDataResults mrDataResults = new MRDataResults();
        mrDataResults.setRaceTable(raceTable);
        mrDataResults.setSeries("f1");

        Formula1Results formula1Results = new Formula1Results();
        formula1Results.setMRData(mrDataResults);


        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        MockWebServer mockWebServer = new MockWebServer();
        mockWebServer.start();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only getCenters request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(formula1Results));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        FormulaOneService service = retrofit.create(FormulaOneService.class);


        //Step6: we create the call to get results
        Call<Formula1Results> call = service.getResult("2020", 2);
        // and we execute the call
        Response<Formula1Results> response = call.execute();

        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        Formula1Results resultResponse = response.body();
        assertEquals(resultResponse.getMRData().getRaceTable().getSeason(), "2020");
        assertEquals(resultResponse.getMRData().getRaceTable().getRaces().size(), 1);
        assertEquals(resultResponse.getMRData().getRaceTable().getRaces().get(0).getResults().size(), 4);
        assertEquals(resultResponse.getMRData().getRaceTable().getRaces().get(0).getResults().
                get(1).getPoints(), result2.getPoints());
        assertEquals(resultResponse.getMRData().getRaceTable().getRaces().get(0).getResults().
                get(2).getIdDriver(), "sainz");
        assertEquals(resultResponse.getMRData().getRaceTable().getRaces().get(0).getCircuit().
                getLocation().getLocality(), "Spielburg");


        //Step9: Finish web server
        mockWebServer.shutdown();

    }

    @Test
    public void getCalendar() throws IOException {

        //Step1: Create instances

        Location location1 = new Location();
        location1.setCountry("Austria");
        location1.setLocality("Spielburg");

        Location location2 = new Location();
        location2.setCountry("Austria");
        location2.setLocality("Spielburg");

        Location location3 = new Location();
        location3.setCountry("UK");
        location3.setLocality("Silverstone");

        Circuit circuit1 = new Circuit();
        circuit1.setCircuitName("Red Bull Ring");
        circuit1.setCircuitId("red_bull_ring");
        circuit1.setLocation(location1);

        Circuit circuit2 = new Circuit();
        circuit2.setCircuitName("Red Bull Ring");
        circuit2.setCircuitId("red_bull_ring");
        circuit2.setLocation(location2);

        Circuit circuit3 = new Circuit();
        circuit3.setCircuitName("Silverstone Circuit");
        circuit3.setCircuitId("silverstone");
        circuit3.setLocation(location3);

        List<Race> raceList = new ArrayList<>();
        Race race1 = new Race();
        race1.setRound("1");
        race1.setCircuit(circuit1);
        race1.setRaceName("Austrian Grand Prix");
        race1.setDate("01/01/2020");
        raceList.add(race1);

        Race race2 = new Race();
        race2.setRound("2");
        race2.setCircuit(circuit2);
        race2.setRaceName("Styrian Grand Prix");
        race2.setDate("02/02/2020");
        raceList.add(race2);

        Race race3 = new Race();
        race3.setRound("3");
        race3.setCircuit(circuit3);
        race3.setRaceName("70th Anniversary Grand Prix");
        race3.setDate("03/03/2020");
        raceList.add(race3);

        RaceTable raceTable = new RaceTable();
        raceTable.setSeason("2020");
        raceTable.setRaces(raceList);

        MRDataCalendar mrDataCalendar = new MRDataCalendar();
        mrDataCalendar.setRaceTable(raceTable);
        mrDataCalendar.setSeries("f1");

        Formula1Calendar formula1Calendar = new Formula1Calendar();
        formula1Calendar.setMRData(mrDataCalendar);


        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        MockWebServer mockWebServer = new MockWebServer();
        mockWebServer.start();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only getCenters request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(formula1Calendar));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        FormulaOneService service = retrofit.create(FormulaOneService.class);


        //Step6: we create the call to get calendar
        Call<Formula1Calendar> call = service.getCalendar("2020");
        // and we execute the call
        Response<Formula1Calendar> response = call.execute();

        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        Formula1Calendar calendarResponse = response.body();
        assertEquals("2020", calendarResponse.getMRData().getRaceTable().getSeason());
        assertEquals(calendarResponse.getMRData().getRaceTable().getRaces().size(), 3);
        assertEquals(calendarResponse.getMRData().getRaceTable().getRaces().get(0).getDate(), "01/01/2020");
        assertEquals(calendarResponse.getMRData().getRaceTable().getRaces().get(1).getCircuit().
                getLocation().getLocality(), "Spielburg");
        assertEquals(calendarResponse.getMRData().getRaceTable().getRaces().get(2).getCircuit().
                getLocation().getLocality(), location3.getLocality());


        //Step9: Finish web server
        mockWebServer.shutdown();

    }

    @Test
    public void getDriverConstructors() throws IOException {

        //Step1: Create instances

        List<Constructor> constructorList = new ArrayList<>();
        Constructor constructor1 = new Constructor();
        constructor1.setConstructorId("force_india");
        constructor1.setName("FORCE INDIA");
        constructorList.add(constructor1);

        Constructor constructor2 = new Constructor();
        constructor2.setConstructorId("renault");
        constructor2.setName("RENAULT");
        constructorList.add(constructor2);

        Constructor constructor3 = new Constructor();
        constructor3.setConstructorId("sauber");
        constructor3.setName("SAUBER");
        constructorList.add(constructor3);

        ConstructorTable constructorTable = new ConstructorTable();
        constructorTable.setDriverId("hulkenberg");
        constructorTable.setConstructors(constructorList);

        MRDataDriversConstructors mrDataDriversConstructors = new MRDataDriversConstructors();
        mrDataDriversConstructors.setConstructorTable(constructorTable);
        mrDataDriversConstructors.setSeries("f1");

        Formula1DriversConstructors formula1DriversConstructors = new Formula1DriversConstructors();
        formula1DriversConstructors.setMRData(mrDataDriversConstructors);


        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        MockWebServer mockWebServer = new MockWebServer();
        mockWebServer.start();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only getCenters request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(formula1DriversConstructors));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        FormulaOneService service = retrofit.create(FormulaOneService.class);


        //Step6: we create the call to get driver constructors
        Call<Formula1DriversConstructors> call = service.getDriverConstructors("hulkenberg");
        // and we execute the call
        Response<Formula1DriversConstructors> response = call.execute();

        //Step7: let's check that the call is executed
        assertNotNull(response);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        Formula1DriversConstructors driversConstructorsResponse = response.body();
        assertEquals(driversConstructorsResponse.getMRData().getConstructorTable().getDriverId(), "hulkenberg");
        assertEquals(driversConstructorsResponse.getMRData().getConstructorTable().getConstructors().size(), 3);
        assertEquals(driversConstructorsResponse.getMRData().getConstructorTable().getConstructors().get(0).getName(), constructor1.getName());
        assertEquals(driversConstructorsResponse.getMRData().getConstructorTable().getConstructors().get(2).getConstructorId(), "sauber");


        //Step9: Finish web server
        mockWebServer.shutdown();

    }
}
